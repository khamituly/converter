package com.example.assignment4;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MoneyConvert extends AppCompatActivity {
    Button convert;
    private TextView num1,res;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_money_convert);
        convert = findViewById(R.id.convertBtn);
        num1 =findViewById(R.id.input);
        res = findViewById(R.id.result);
        convert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                convert();
            }
        });
    }

     void convert() {

         String valueEnteredInTg = num1.getText().toString();
         double Tenge = Double.parseDouble(valueEnteredInTg);
         double Dollar = Tenge* 0.0024;
         String result = String.valueOf(Dollar)+'$';
         res.setText(result);
         Toast.makeText(this, "Result is: " +result,
                 Toast.LENGTH_SHORT).show();
    }


}