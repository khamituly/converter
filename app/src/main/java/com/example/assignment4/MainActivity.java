package com.example.assignment4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button money,temp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        money = (Button) findViewById(R.id.curn);
        money.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redirectCurn();
            }
        });

        temp = (Button) findViewById(R.id.temp);
        temp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redirectTemp();
            }
        });
    }

    void redirectCurn(){
        Intent intent =new Intent(this, MoneyConvert.class);
        startActivity(intent);
    }

    void redirectTemp(){
        Intent intent = new Intent(this,TemperatureActivity.class);
        startActivity(intent);
    }

}