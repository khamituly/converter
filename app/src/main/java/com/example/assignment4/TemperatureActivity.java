package com.example.assignment4;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;



public class TemperatureActivity extends AppCompatActivity {

    private TextView textTemperatures, resultTemp;
    private EditText enterTemp;
    private Button CtoF,CtoK,FtoC,FtoK,KtoC,KtoF;
    double result0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temperature);

        textTemperatures = findViewById(R.id.textTemperatures);
        resultTemp = findViewById(R.id.resultTemp);

        enterTemp = findViewById(R.id.enterTemp);

        CtoF = findViewById(R.id.CtoF);
        CtoK = findViewById(R.id.CtoK);
        FtoC = findViewById(R.id.FtoC);
        FtoK = findViewById(R.id.FtoK);
        KtoC = findViewById(R.id.KtoC);
        KtoF = findViewById(R.id.KtoF);


        CtoF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConvertCtoF();
            }
        });
        CtoK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConvertCtoK();
            }
        });
        FtoC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConvertFtoC();
            }
        });
        FtoK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConvertFtoK();
            }
        });
        KtoC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConvertKtoC();
            }
        });
        KtoF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConvertKtoF();
            }
        });
    }

    private void ConvertKtoF() {

        double temp = Double.parseDouble(enterTemp.getText().toString());
        result0 = (temp - 273.15) * 9/5 + 32;
        resultTemp.setText(String.valueOf(result0));
    }

    private void ConvertKtoC() {

        double temp = Double.parseDouble(enterTemp.getText().toString());
        result0 = temp - 273.15;
        resultTemp.setText(String.valueOf(result0));
    }

    private void ConvertFtoK() {

        double temp = Double.parseDouble(enterTemp.getText().toString());
        result0 = (temp - 32) * 5/9 + 273.15;
        resultTemp.setText(String.valueOf(result0));
    }

    private void ConvertFtoC() {

        double temp = Double.parseDouble(enterTemp.getText().toString());
        result0 = (temp - 32) / 1.8;
        resultTemp.setText(String.valueOf(result0));
    }

    private void ConvertCtoK() {

        double temp = Double.parseDouble(enterTemp.getText().toString());
        result0 = temp + 273.15;
        resultTemp.setText(String.valueOf(result0));
    }

    private void ConvertCtoF() {

        double temp = Double.parseDouble(enterTemp.getText().toString());
        result0 = (temp * 1.8) + 32;
        resultTemp.setText(String.valueOf(result0));
    }

}